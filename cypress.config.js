const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
  },
  env: {
    CalculatorUrl: 'https://www.calkoo.com/en/vat-calculator',
  }
});
