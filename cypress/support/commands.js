const scope = '#vatcalculator';

Cypress.Commands.add('visitCalculator', (originalFn, url, options) => {
    cy.visit(Cypress.env('CalculatorUrl'));
})

Cypress.Commands.add('selectCalculationTypeByLabel', (label) => {
    cy.get(scope).contains('label', label).click();
})


// set value to the chosen calculation input
Cypress.Commands.add('inputValue', (id, text) => {
    cy.get(scope).get('#' + id).type(text);
})

// get the data from the calculator in the following format
// { netPrice: number, vat: number, price: number } 
Cypress.Commands.add('getCalculatorData', () => {

    var data = {}

    return cy.get(scope).get('#NetPrice')
        .then(input => data.netPrice = parseInt(input.val()))
        .then(() => cy.get(scope).get('#VATsum'))
        .then(input => data.vat = parseInt(input.val()))
        .then(() => cy.get(scope).get('#Price'))
        .then(input => data.price = parseInt(input.val()))
        .then(() => data);
})

