describe('Test happy scenario calculations', () => {

  const priceWithoutVAT = 10;
  const VAT = 2;
  const priceInclVat = priceWithoutVAT + VAT;

  it('Ceed price without VAT', () => {
    cy.visitCalculator();
    cy.selectCalculationTypeByLabel('Price without VAT');
    cy.inputValue('NetPrice', priceWithoutVAT);
    cy.getCalculatorData().its('vat').should('eq', VAT);
    cy.getCalculatorData().its('price').should('eq', priceInclVat);
  })

  it('Ceed value-added tax', () => {
    cy.visitCalculator();
    cy.selectCalculationTypeByLabel('Value-Added Tax');
    cy.inputValue('VATsum', VAT);
    cy.getCalculatorData().its('netPrice').should('eq', priceWithoutVAT);
    cy.getCalculatorData().its('price').should('eq', priceInclVat);
  })

  it('Ceed price incl. VAT', () => {
    cy.visitCalculator();
    cy.selectCalculationTypeByLabel('Price incl. VAT');
    cy.inputValue('Price', priceInclVat);
    cy.getCalculatorData().its('vat').should('eq', VAT);
    cy.getCalculatorData().its('netPrice').should('eq', priceWithoutVAT);
  })
})